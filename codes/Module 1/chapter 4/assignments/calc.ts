var num1 : HTMLInputElement = <HTMLInputElement>document.getElementById("number1");
 
 var num2 : HTMLInputElement = <HTMLInputElement>document.getElementById("number2");

 var num3 : HTMLInputElement = <HTMLInputElement>document.getElementById("result");

 function add() {
  var c:number = parseFloat(num1.value) + parseFloat(num2.value);
  num3.value = c.toString();
 }

 function subtract() {
  var c:number = parseFloat(num1.value) - parseFloat(num2.value);
  num3.value = c.toString();
 }
 
 function multiply() {
  var c:number = parseFloat(num1.value) * parseFloat(num2.value);
  num3.value = c.toString();
 }

 function divide() {
  var c:number = parseFloat(num1.value) / parseFloat(num2.value);
  num3.value = c.toString();
 }